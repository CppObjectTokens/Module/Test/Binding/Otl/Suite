/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <Test/Token/Bind.h>
#include <Test/Token/Feature.h>

#include <Test/Binding/Otn/CppLang/Basis.h>

#include <otn/cpp_lang.hpp>
#include <otn/v1/tag/itself.hpp>

namespace Test
{

namespace Bind
{

namespace Internal
{

// ----- Itself ----------------------------------------------------------------
template <>
struct Itself<Test::Basis::CppLang>
{
    using Type = otn::itself_t;
    static constexpr Type Value = otn::itself;
};

template <class Element>
struct ItselfType<Element, Test::Basis::CppLang>
{
    using Type = otn::itself_type_t<Element>;
    static constexpr Type Value = otn::itself_type<Element>;
};
// ----- Itself ----------------------------------------------------------------

// ----- Instance --------------------------------------------------------------
template <class Element>
struct Instance<Element,
                Test::Basis::CppLang,
                Test::Ownership::Unified,
                Test::Multiplicity::Unknown>
{ using Type = otn::cpp_lang::unified_unknown<Element>; };

template <class Element>
struct Instance<Element,
                Test::Basis::CppLang,
                Test::Ownership::Unified,
                Test::Multiplicity::Optional>
{ using Type = otn::cpp_lang::unified_optional<Element>; };
// ----- Instance --------------------------------------------------------------

namespace Rule
{

// ----- Spec ------------------------------------------------------------------
template <class Token>
struct Spec<Token, IsInstanceOf<Token,
                                Test::Basis::CppLang,
                                Test::Ownership::Unified,
                                Test::Multiplicity::Unknown>>
    : TokenSpec<Token,
                Test::Basis::CppLang,
                Test::Ownership::Unified,
                Test::Multiplicity::Unknown> {};

template <class Token>
struct Spec<Token, IsInstanceOf<Token,
                                Test::Basis::CppLang,
                                Test::Ownership::Unified,
                                Test::Multiplicity::Optional>>
    : TokenSpec<Token,
                Test::Basis::CppLang,
                Test::Ownership::Unified,
                Test::Multiplicity::Optional> {};
// ----- Spec ------------------------------------------------------------------

// ----- Lifetime --------------------------------------------------------------
template <>
struct Lifetime<Test::Basis::CppLang, Test::Ownership::Unified>
{
    static constexpr auto Role        = Test::Lifetime::Role::Observer;
    static constexpr bool IsTrackable = false;
};
// ----- Lifetime --------------------------------------------------------------

// ----- IsPointerConstructible ------------------------------------------------
template <>
inline constexpr bool
IsPointerConstructible<Test::Basis::CppLang, Test::Ownership::Unified> = true;
// ----- IsPointerConstructible ------------------------------------------------

// ----- IsItselfConstructible -------------------------------------------------
template <>
inline constexpr bool
IsItselfConstructible<Test::Basis::CppLang, Test::Ownership::Unified> = false;
// ----- IsItselfConstructible -------------------------------------------------

// ----- Copy construction & assignment ----------------------------------------
// ----- CppLang <- CppLang ----------------------------------------------------
// CppLang::UnifiedUnknown<T> <- CppLang::SomeSome<Y>
template <>
struct
CanCopy<Test::Basis::CppLang, Test::Ownership::Unified, Test::Multiplicity::Unknown,
        Test::Basis::CppLang, Test::Ownership::Unified, Test::Multiplicity::Unknown> : ImplicitCopy {};

template <>
struct
CanCopy<Test::Basis::CppLang, Test::Ownership::Unified, Test::Multiplicity::Unknown,
        Test::Basis::CppLang, Test::Ownership::Unified, Test::Multiplicity::Optional> : ImplicitCopy {};

// CppLang::UnifiedOptional<T> <- CppLang::SomeSome<Y>
template <>
struct
CanCopy<Test::Basis::CppLang, Test::Ownership::Unified, Test::Multiplicity::Optional,
        Test::Basis::CppLang, Test::Ownership::Unified, Test::Multiplicity::Unknown> : ImplicitCopy {};

template <>
struct
CanCopy<Test::Basis::CppLang, Test::Ownership::Unified, Test::Multiplicity::Optional,
        Test::Basis::CppLang, Test::Ownership::Unified, Test::Multiplicity::Optional> : ImplicitCopy {};
// ----- CppLang <- CppLang ----------------------------------------------------
// ----- Copy construction & assignment ----------------------------------------

// ----- Move construction & assignment ----------------------------------------
// ----- CppLang <- CppLang ----------------------------------------------------
// CppLang::UnifiedUnknown<T> <- CppLang::UnifiedSome<Y>
template <>
struct
CanMove<Test::Basis::CppLang, Test::Ownership::Unified, Test::Multiplicity::Unknown,
        Test::Basis::CppLang, Test::Ownership::Unified, Test::Multiplicity::Unknown> : ImplicitMoveUnspecified {};

template <>
struct
CanMove<Test::Basis::CppLang, Test::Ownership::Unified, Test::Multiplicity::Unknown,
        Test::Basis::CppLang, Test::Ownership::Unified, Test::Multiplicity::Optional> : ImplicitMove {};

// CppLang::UnifiedOptional<T> <- CppLang::UnifiedSome<Y>
template <>
struct
CanMove<Test::Basis::CppLang, Test::Ownership::Unified, Test::Multiplicity::Optional,
        Test::Basis::CppLang, Test::Ownership::Unified, Test::Multiplicity::Unknown> : ImplicitMoveUnspecified {};

template <>
struct
CanMove<Test::Basis::CppLang, Test::Ownership::Unified, Test::Multiplicity::Optional,
        Test::Basis::CppLang, Test::Ownership::Unified, Test::Multiplicity::Optional> : ImplicitMove {};
// ----- CppLang <- CppLang ----------------------------------------------------
// ----- Move construction & assignment ----------------------------------------

// ----- HasFeature ------------------------------------------------------------
template <>
inline constexpr bool
HasFeature<Test::Basis::CppLang, Test::Ownership::Unified, Test::Multiplicity::Optional,
           Test::Feature::IsRange> = true;
// ----- HasFeature ------------------------------------------------------------

} // namespace Rule

} // namespace Internal

} // namespace Bind

} // namespace Test
