/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <Test/Token/Bind.h>
#include <Test/Token/Ownership.h>
#include <Test/Token/Multiplicity.h>
#include <Test/Token/Lifetime.h>

#include <Test/Binding/Otn/CppLang/Basis.h>
#include <Test/Binding/Otn/Safe/Basis.h>
#include <Test/Binding/Otn/Slim/Basis.h>
#include <Test/Binding/Otn/Raw/Basis.h>
#include <Test/Binding/StdSmart/Origin/Basis.h>

#include <otn/v1/ownership/names.hpp>
#include <otn/v1/multiplicity/names.hpp>
#include <otn/v1/lifetime/names.hpp>

#include <otn/v1/cpp_lang/basis.hpp>
#include <otn/v1/safe/basis.hpp>
#include <otn/v1/slim/basis.hpp>
#include <otn/v1/raw/basis.hpp>
#include <otn/v1/std_smart/basis.hpp>

namespace Test
{

namespace Bind
{

namespace Internal
{

// ----- Gateway ---------------------------------------------------------------
// ----- Outer -----------------------------------------------------------------
// ----- Basis -----------------------------------------------------------------
template <>
struct Gateway<otn::basis::cpp_lang>
{
    using Inner = Test::Basis::CppLang;
    using Outer = otn::basis::cpp_lang;
};

template <>
struct Gateway<otn::basis::safe>
{
    using Inner = Test::Basis::OtnSafe;
    using Outer = otn::basis::safe;
};

template <>
struct Gateway<otn::basis::slim>
{
    using Inner = Test::Basis::OtnSlim;
    using Outer = otn::basis::slim;
};

template <>
struct Gateway<otn::basis::raw>
{
    using Inner = Test::Basis::OtnRaw;
    using Outer = otn::basis::raw;
};

template <>
struct Gateway<otn::basis::std_smart>
{
    using Inner = Test::Basis::StdSmart;
    using Outer = otn::basis::std_smart;
};
// ----- Basis -----------------------------------------------------------------

// ----- Ownership -------------------------------------------------------------
template <>
struct Gateway<otn::ownership::unified>
{
    using Inner = Test::Ownership::Unified;
    using Outer = otn::ownership::unified;
};

template <>
struct Gateway<otn::ownership::unique>
{
    using Inner = Test::Ownership::Unique;
    using Outer = otn::ownership::unique;
};

template <>
struct Gateway<otn::ownership::shared>
{
    using Inner = Test::Ownership::Shared;
    using Outer = otn::ownership::shared;
};

template <>
struct Gateway<otn::ownership::weak>
{
    using Inner = Test::Ownership::Weak;
    using Outer = otn::ownership::weak;
};
// ----- Ownership -------------------------------------------------------------

// ----- Multiplicity ----------------------------------------------------------
template <>
struct Gateway<otn::multiplicity::unknown>
{
    using Inner = Test::Multiplicity::Unknown;
    using Outer = otn::multiplicity::unknown;
};

template <>
struct Gateway<otn::multiplicity::optional>
{
    using Inner = Test::Multiplicity::Optional;
    using Outer = otn::multiplicity::optional;
};

template <>
struct Gateway<otn::multiplicity::single>
{
    using Inner = Test::Multiplicity::Single;
    using Outer = otn::multiplicity::single;
};
// ----- Multiplicity ----------------------------------------------------------

// ----- Lifetime --------------------------------------------------------------
template <>
struct Gateway<otn::lifetime::role>
{
    static constexpr auto InnerValue(otn::lifetime::role value)
    { return static_cast<Test::Lifetime::Role>(value); }
    static constexpr auto OuterValue(otn::lifetime::role value)
    { return value; }
};
// ----- Lifetime --------------------------------------------------------------
// ----- Outer -----------------------------------------------------------------

// ----- Inner -----------------------------------------------------------------
// ----- Basis -----------------------------------------------------------------
template <>
struct Gateway<Test::Basis::CppLang>
{
    using Inner = Test::Basis::CppLang;
    using Outer = otn::basis::cpp_lang;
};

template <>
struct Gateway<Test::Basis::OtnSafe>
{
    using Inner = Test::Basis::OtnSafe;
    using Outer = otn::basis::safe;
};

template <>
struct Gateway<Test::Basis::OtnSlim>
{
    using Inner = Test::Basis::OtnSlim;
    using Outer = otn::basis::slim;
};

template <>
struct Gateway<Test::Basis::OtnRaw>
{
    using Inner = Test::Basis::OtnRaw;
    using Outer = otn::basis::raw;
};

template <>
struct Gateway<Test::Basis::StdSmart>
{
    using Inner = Test::Basis::StdSmart;
    using Outer = otn::basis::std_smart;
};
// ----- Basis -----------------------------------------------------------------

// ----- Ownership -------------------------------------------------------------
template <>
struct Gateway<Test::Ownership::Unified>
{
    using Inner = Test::Ownership::Unified;
    using Outer = otn::ownership::unified;
};

template <>
struct Gateway<Test::Ownership::Unique>
{
    using Inner = Test::Ownership::Unique;
    using Outer = otn::ownership::unique;
};

template <>
struct Gateway<Test::Ownership::Shared>
{
    using Inner = Test::Ownership::Shared;
    using Outer = otn::ownership::shared;
};

template <>
struct Gateway<Test::Ownership::Weak>
{
    using Inner = Test::Ownership::Weak;
    using Outer = otn::ownership::weak;
};
// ----- Ownership -------------------------------------------------------------

// ----- Multiplicity ----------------------------------------------------------
template <>
struct Gateway<Test::Multiplicity::Unknown>
{
    using Inner = Test::Multiplicity::Unknown;
    using Outer = otn::multiplicity::unknown;
};

template <>
struct Gateway<Test::Multiplicity::Optional>
{
    using Inner = Test::Multiplicity::Optional;
    using Outer = otn::multiplicity::optional;
};

template <>
struct Gateway<Test::Multiplicity::Single>
{
    using Inner = Test::Multiplicity::Single;
    using Outer = otn::multiplicity::single;
};
// ----- Multiplicity ----------------------------------------------------------

// ----- Lifetime --------------------------------------------------------------
template <>
struct Gateway<Test::Lifetime::Role>
{
    static constexpr auto InnerValue(Test::Lifetime::Role value)
    { return value; }
    static constexpr auto OuterValue(Test::Lifetime::Role value)
    { return static_cast<otn::lifetime::role>(value); }
};
// ----- Lifetime --------------------------------------------------------------
// ----- Inner -----------------------------------------------------------------
// ----- Gateway ---------------------------------------------------------------

} // namespace Internal

} // namespace Bind

} // namespace Test
