/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <Test/Token/Bind.h>

#include <otn/access.hpp>

namespace Test
{

namespace Bind
{

namespace Internal
{

namespace Helper
{

template <class Basis>
struct Access<Basis>
{
    template <class ... Args>
    static auto access(Args&& ... args)
    { return otn::access(std::forward<Args>(args)...); }
};

template <class Basis>
struct Gain<Basis>
{
    template <class ... Tokens>
    [[nodiscard]] static
    auto gain(Tokens&& ... tokens) noexcept
    { return otn::gain(std::forward<Tokens>(tokens)...); }

    static void gain() = delete;
};

} // namespace Helper

} // namespace Internal

} // namespace Bind

} // namespace Test
