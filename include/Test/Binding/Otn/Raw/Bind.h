/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <Test/Token/Lifetime.h>
#include <Test/Token/Feature.h>

#include <Test/Binding/Otn/Gateway.h>
#include <Test/Binding/Otn/Raw/Basis.h>

#include <otn/raw.hpp>

namespace Test
{

namespace Bind
{

namespace Internal
{

// ----- Itself ----------------------------------------------------------------
template <>
struct Itself<Test::Basis::OtnRaw>
{
    using Type = otn::itself_t;
    static constexpr Type Value = otn::itself;
};

template <class Element>
struct ItselfType<Element, Test::Basis::OtnRaw>
{
    using Type = otn::itself_type_t<Element>;
    static constexpr Type Value = otn::itself_type<Element>;
};
// ----- Itself ----------------------------------------------------------------

// ----- Instance --------------------------------------------------------------
template <class Element, class Multiplicity>
struct Instance<Element, Test::Basis::OtnRaw, Test::Ownership::Unified, Multiplicity>
{
    using Type = otn::generic::token<Element,
                                     otn::basis::raw,
                                     Bind::Gateway::Outer<Test::Ownership::Unified>,
                                     Bind::Gateway::Outer<Multiplicity>>;
};

template <class Element, class Multiplicity>
struct Instance<Element, Test::Basis::OtnRaw, Test::Ownership::Unique, Multiplicity,
                std::enable_if_t<!std::is_abstract_v<Element>>>
{
    using Type = otn::generic::token<Element,
                                     otn::basis::raw,
                                     Bind::Gateway::Outer<Test::Ownership::Unique>,
                                     Bind::Gateway::Outer<Multiplicity>>;
};

template <class Element, class Multiplicity>
struct Instance<Element, Test::Basis::OtnRaw, Test::Ownership::Weak, Multiplicity>
{
    using Type = otn::generic::token<Element,
                                     otn::basis::raw,
                                     Bind::Gateway::Outer<Test::Ownership::Weak>,
                                     Bind::Gateway::Outer<Multiplicity>>;
};
// ----- Instance --------------------------------------------------------------

namespace Rule
{

// ----- Spec ------------------------------------------------------------------
template <class Token>
struct Spec<Token, IsInstanceOf<Token,
                                Test::Basis::OtnRaw,
                                Test::Ownership::Unified,
                                Test::Multiplicity::Optional>>
    : TokenSpec<Token,
                Test::Basis::OtnRaw,
                Test::Ownership::Unified,
                Test::Multiplicity::Optional> {};

template <class Token>
struct Spec<Token, IsInstanceOf<Token,
                                Test::Basis::OtnRaw,
                                Test::Ownership::Unified,
                                Test::Multiplicity::Single>>
    : TokenSpec<Token,
                Test::Basis::OtnRaw,
                Test::Ownership::Unified,
                Test::Multiplicity::Single> {};

template <class Token>
struct Spec<Token, IsInstanceOf<Token,
                                Test::Basis::OtnRaw,
                                Test::Ownership::Unique,
                                Test::Multiplicity::Optional>>
    : TokenSpec<Token,
                Test::Basis::OtnRaw,
                Test::Ownership::Unique,
                Test::Multiplicity::Optional> {};

template <class Token>
struct Spec<Token, IsInstanceOf<Token,
                                Test::Basis::OtnRaw,
                                Test::Ownership::Unique,
                                Test::Multiplicity::Single>>
    : TokenSpec<Token,
                Test::Basis::OtnRaw,
                Test::Ownership::Unique,
                Test::Multiplicity::Single> {};

template <class Token>
struct Spec<Token, IsInstanceOf<Token,
                                Test::Basis::OtnRaw,
                                Test::Ownership::Weak,
                                Test::Multiplicity::Optional>>
    : TokenSpec<Token,
                Test::Basis::OtnRaw,
                Test::Ownership::Weak,
                Test::Multiplicity::Optional> {};

template <class Token>
struct Spec<Token, IsInstanceOf<Token,
                                Test::Basis::OtnRaw,
                                Test::Ownership::Weak,
                                Test::Multiplicity::Single>>
    : TokenSpec<Token,
                Test::Basis::OtnRaw,
                Test::Ownership::Weak,
                Test::Multiplicity::Single> {};
// ----- Spec ------------------------------------------------------------------

// ----- Lifetime --------------------------------------------------------------
template <>
struct Lifetime<Test::Basis::OtnRaw, Test::Ownership::Unified>
{
    static constexpr auto Role        = Test::Lifetime::Role::Observer;
    static constexpr bool IsTrackable = false;
};

template <>
struct Lifetime<Test::Basis::OtnRaw, Test::Ownership::Unique>
{
    static constexpr auto Role        = Test::Lifetime::Role::Owner;
    static constexpr bool IsTrackable = false;
};

template <>
struct Lifetime<Test::Basis::OtnRaw, Test::Ownership::Weak>
{
    static constexpr auto Role        = Test::Lifetime::Role::Observer;
    static constexpr bool IsTrackable = false;
};
// ----- Lifetime --------------------------------------------------------------

// ----- IsPointerConstructible ------------------------------------------------
template <>
inline constexpr bool
IsPointerConstructible<Test::Basis::OtnRaw, Test::Ownership::Unified> = true;

template <>
inline constexpr bool
IsPointerConstructible<Test::Basis::OtnRaw, Test::Ownership::Weak> = true;
// ----- IsPointerConstructible ------------------------------------------------

// ----- IsItselfConstructible -------------------------------------------------
template <>
inline constexpr bool
IsItselfConstructible<Test::Basis::OtnRaw, Test::Ownership::Unified> = false;

template <>
inline constexpr bool
IsItselfConstructible<Test::Basis::OtnRaw, Test::Ownership::Unique> = true;

template <>
inline constexpr bool
IsItselfTypeConstructible<Test::Basis::OtnRaw, Test::Ownership::Unique,
                          Test::Multiplicity::Optional> = false;

template <>
inline constexpr bool
IsItselfConstructible<Test::Basis::OtnRaw, Test::Ownership::Weak> = false;
// ----- IsItselfConstructible -------------------------------------------------

// ----- Copy construction & assignment ----------------------------------------
// ----- OtnRaw <- OtnRaw ------------------------------------------------------
// OtnRaw::UnifiedAny<T> <- OtnRaw::AnyAny<Y>
template <class TM,
          class YO, class YM>
struct
CanCopy<Test::Basis::OtnRaw, Test::Ownership::Unified, TM,
        Test::Basis::OtnRaw, YO, YM> : ImplicitCopy {};

// OtnRaw::WeakAny<T> <- OtnRaw::SomeAny<Y>
template <class TM,
          class YM>
struct
CanCopy<Test::Basis::OtnRaw, Test::Ownership::Weak, TM,
        Test::Basis::OtnRaw, Test::Ownership::Unique, YM> : ImplicitCopy {};

template <class TM,
          class YM>
struct
CanCopy<Test::Basis::OtnRaw, Test::Ownership::Weak, TM,
        Test::Basis::OtnRaw, Test::Ownership::Weak, YM> : ImplicitCopy {};
// ----- OtnRaw <- OtnRaw ------------------------------------------------------
// ----- Copy construction & assignment ----------------------------------------

// ----- Move construction & assignment ----------------------------------------
// ----- OtnRaw <- OtnRaw ------------------------------------------------------
// OtnRaw::UnifiedAny<T> <- OtnRaw::SomeSome<Y>
template <class TM>
struct
CanMove<Test::Basis::OtnRaw, Test::Ownership::Unified, TM,
        Test::Basis::OtnRaw, Test::Ownership::Unified, Test::Multiplicity::Optional> : ImplicitMove {};

template <class TM>
struct
CanMove<Test::Basis::OtnRaw, Test::Ownership::Unified, TM,
        Test::Basis::OtnRaw, Test::Ownership::Unified, Test::Multiplicity::Single> : ImplicitMoveUnspecified {};

template <class TM>
struct
CanMove<Test::Basis::OtnRaw, Test::Ownership::Unified, TM,
        Test::Basis::OtnRaw, Test::Ownership::Weak, Test::Multiplicity::Optional> : ImplicitMove {};

template <class TM>
struct
CanMove<Test::Basis::OtnRaw, Test::Ownership::Unified, TM,
        Test::Basis::OtnRaw, Test::Ownership::Weak, Test::Multiplicity::Single> : ImplicitMoveUnspecified {};

// OtnRaw::UniqueSome<T> <- OtnRaw::UniqueSome<Y>
template <class TM>
struct
CanMove<Test::Basis::OtnRaw, Test::Ownership::Unique, TM,
        Test::Basis::OtnRaw, Test::Ownership::Unique, Test::Multiplicity::Optional> : ImplicitMove
{
    static constexpr bool IsRelocateObject = true;
};

template <class TM>
struct
CanMove<Test::Basis::OtnRaw, Test::Ownership::Unique, TM,
        Test::Basis::OtnRaw, Test::Ownership::Unique, Test::Multiplicity::Single> : ImplicitMoveRelocateObject {};

// OtnRaw::WeakAny<T> <- OtnRaw::SomeSome<Y>
template <class TM>
struct
CanMove<Test::Basis::OtnRaw, Test::Ownership::Weak, TM,
        Test::Basis::OtnRaw, Test::Ownership::Weak, Test::Multiplicity::Optional> : ImplicitMove {};

template <class TM>
struct
CanMove<Test::Basis::OtnRaw, Test::Ownership::Weak, TM,
        Test::Basis::OtnRaw, Test::Ownership::Weak, Test::Multiplicity::Single> : ImplicitMoveUnspecified {};
// ----- OtnRaw <- OtnRaw ------------------------------------------------------
// ----- Move construction & assignment ----------------------------------------

// ----- HasFeature ------------------------------------------------------------
template <class Ownership, class Multiplicity>
inline constexpr bool
HasFeature<Test::Basis::OtnRaw, Ownership, Multiplicity,
           Test::Feature::IsRange> = true;

template <class Ownership, class Multiplicity>
inline constexpr bool
HasFeature<Test::Basis::OtnRaw, Ownership, Multiplicity,
           Test::Feature::ContractViolation::Abort> = true;
// ----- HasFeature ------------------------------------------------------------

} // namespace Rule

} // namespace Internal

} // namespace Bind

} // namespace Test
