/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <Test/Token/Bind.h>

#include <Test/Binding/Otn/Raw/Basis.h>
#include <Test/Binding/Otn/CppLang/Basis.h>

namespace Test
{

namespace Bind
{

namespace Internal
{

namespace Rule
{

// ----- Copy construction & assignment ----------------------------------------
// ----- OtnRaw <- CppLang -----------------------------------------------------
// OtnRaw::UnifiedAny<T> <- CppLang::AnyAny<Y>
template <class TM,
          class YO, class YM>
struct
CanCopy<Test::Basis::OtnRaw, Test::Ownership::Unified, TM,
        Test::Basis::CppLang, YO, YM> : ImplicitCopy {};

// OtnRaw::WeakAny<T> <- CppLang::UnifiedUnknown<Y>
template <class TM>
struct
CanCopy<Test::Basis::OtnRaw, Test::Ownership::Weak, TM,
        Test::Basis::CppLang, Test::Ownership::Unified, Test::Multiplicity::Unknown> : ExplicitCopy {};
// ----- OtnRaw <- CppLang -----------------------------------------------------

// ----- CppLang <- OtnRaw -----------------------------------------------------
// CppLang::UnifiedUnknown<T> <- OtnRaw::AnyAny<Y>
template <class YO, class YM>
struct
CanCopy<Test::Basis::CppLang, Test::Ownership::Unified, Test::Multiplicity::Unknown,
        Test::Basis::OtnRaw, YO, YM> : ExplicitCopy {};

// CppLang::UnifiedOptional<T> <- OtnRaw::AnyAny<Y>
template <class YO, class YM>
struct
CanCopy<Test::Basis::CppLang, Test::Ownership::Unified, Test::Multiplicity::Optional,
        Test::Basis::OtnRaw, YO, YM> : ImplicitCopy {};
// ----- CppLang <- OtnRaw -----------------------------------------------------
// ----- Copy construction & assignment ----------------------------------------

// ----- Move construction & assignment ----------------------------------------
// ----- OtnRaw <- CppLang -----------------------------------------------------
// OtnRaw::UnifiedAny<T> <- CppLang::UnifiedUnknown<Y>
template <class TM>
struct
CanMove<Test::Basis::OtnRaw, Test::Ownership::Unified, TM,
        Test::Basis::CppLang, Test::Ownership::Unified, Test::Multiplicity::Unknown> : ImplicitMoveUnspecified {};

// OtnRaw::UnifiedAny<T> <- CppLang::UnifiedOptional<Y>
template <class TM>
struct
CanMove<Test::Basis::OtnRaw, Test::Ownership::Unified, TM,
        Test::Basis::CppLang, Test::Ownership::Unified, Test::Multiplicity::Optional> : ImplicitMove {};

// OtnRaw::WeakAny<T> <- CppLang::UnifiedUnknown<Y>
template <class TM>
struct
CanMove<Test::Basis::OtnRaw, Test::Ownership::Weak, TM,
        Test::Basis::CppLang, Test::Ownership::Unified, Test::Multiplicity::Unknown> : ExplicitMoveUnspecified {};
// ----- OtnRaw <- CppLang -----------------------------------------------------

// ----- CppLang <- OtnRaw -----------------------------------------------------
// CppLang::UnifiedUnknown<T> <- OtnRaw::SomeAny<Y>
template <class YM>
struct
CanMove<Test::Basis::CppLang, Test::Ownership::Unified, Test::Multiplicity::Unknown,
        Test::Basis::OtnRaw, Test::Ownership::Unified, YM> : ExplicitMove {};

template <class YM>
struct
CanMove<Test::Basis::CppLang, Test::Ownership::Unified, Test::Multiplicity::Unknown,
        Test::Basis::OtnRaw, Test::Ownership::Weak, YM> : ExplicitMove {};

// CppLang::UnifiedOptional<T> <- OtnRaw::SomeAny<Y>
template <class YM>
struct
CanMove<Test::Basis::CppLang, Test::Ownership::Unified, Test::Multiplicity::Optional,
        Test::Basis::OtnRaw, Test::Ownership::Unified, YM> : ImplicitMove {};

template <class YM>
struct
CanMove<Test::Basis::CppLang, Test::Ownership::Unified, Test::Multiplicity::Optional,
        Test::Basis::OtnRaw, Test::Ownership::Weak, YM> : ImplicitMove {};
// ----- CppLang <- OtnRaw -----------------------------------------------------
// ----- Move construction & assignment ----------------------------------------

} // namespace Rule

} // namespace Internal

} // namespace Bind

} // namespace Test
