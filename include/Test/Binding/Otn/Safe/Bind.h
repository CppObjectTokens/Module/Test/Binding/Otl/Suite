/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <Test/Token/Lifetime.h>
#include <Test/Token/Feature.h>

#include <Test/Binding/Otn/Gateway.h>
#include <Test/Binding/Otn/Safe/Basis.h>

#include <otn/all.hpp>

namespace Test
{

namespace Bind
{

namespace Internal
{

// ----- Itself ----------------------------------------------------------------
template <>
struct Itself<Test::Basis::OtnSafe>
{
    using Type = otn::itself_t;
    static constexpr Type Value = otn::itself;
};

template <class Element>
struct ItselfType<Element, Test::Basis::OtnSafe>
{
    using Type = otn::itself_type_t<Element>;
    static constexpr Type Value = otn::itself_type<Element>;
};
// ----- Itself ----------------------------------------------------------------

// ----- Instance --------------------------------------------------------------
template <class Element, class Ownership, class Multiplicity>
struct Instance<Element, Test::Basis::OtnSafe, Ownership, Multiplicity>
{
    using Type = otn::generic::token<Element,
                                     otn::basis::safe,
                                     Bind::Gateway::Outer<Ownership>,
                                     Bind::Gateway::Outer<Multiplicity>>;
};
// ----- Instance --------------------------------------------------------------

namespace Rule
{

// ----- Spec ------------------------------------------------------------------
template <class Token>
struct Spec<Token, IsInstanceOf<Token,
                                Test::Basis::OtnSafe,
                                Test::Ownership::Unified,
                                Test::Multiplicity::Optional>>
    : TokenSpec<Token,
                Test::Basis::OtnSafe,
                Test::Ownership::Unified,
                Test::Multiplicity::Optional> {};

template <class Token>
struct Spec<Token, IsInstanceOf<Token,
                                Test::Basis::OtnSafe,
                                Test::Ownership::Unified,
                                Test::Multiplicity::Single>>
    : TokenSpec<Token,
                Test::Basis::OtnSafe,
                Test::Ownership::Unified,
                Test::Multiplicity::Single> {};

template <class Token>
struct Spec<Token, IsInstanceOf<Token,
                                Test::Basis::OtnSafe,
                                Test::Ownership::Unique,
                                Test::Multiplicity::Optional>>
    : TokenSpec<Token,
                Test::Basis::OtnSafe,
                Test::Ownership::Unique,
                Test::Multiplicity::Optional> {};

template <class Token>
struct Spec<Token, IsInstanceOf<Token,
                                Test::Basis::OtnSafe,
                                Test::Ownership::Unique,
                                Test::Multiplicity::Single>>
    : TokenSpec<Token,
                Test::Basis::OtnSafe,
                Test::Ownership::Unique,
                Test::Multiplicity::Single> {};

template <class Token>
struct Spec<Token, IsInstanceOf<Token,
                                Test::Basis::OtnSafe,
                                Test::Ownership::Shared,
                                Test::Multiplicity::Optional>>
    : TokenSpec<Token,
                Test::Basis::OtnSafe,
                Test::Ownership::Shared,
                Test::Multiplicity::Optional> {};

template <class Token>
struct Spec<Token, IsInstanceOf<Token,
                                Test::Basis::OtnSafe,
                                Test::Ownership::Shared,
                                Test::Multiplicity::Single>>
    : TokenSpec<Token,
                Test::Basis::OtnSafe,
                Test::Ownership::Shared,
                Test::Multiplicity::Single> {};

template <class Token>
struct Spec<Token, IsInstanceOf<Token,
                                Test::Basis::OtnSafe,
                                Test::Ownership::Weak,
                                Test::Multiplicity::Optional>>
    : TokenSpec<Token,
                Test::Basis::OtnSafe,
                Test::Ownership::Weak,
                Test::Multiplicity::Optional> {};
// ----- Spec ------------------------------------------------------------------

// ----- Lifetime --------------------------------------------------------------
template <>
struct Lifetime<Test::Basis::OtnSafe, Test::Ownership::Unified>
{
    static constexpr auto Role        = Test::Lifetime::Role::Owner;
    static constexpr bool IsTrackable = true;
};

template <>
struct Lifetime<Test::Basis::OtnSafe, Test::Ownership::Unique>
{
    static constexpr auto Role        = Test::Lifetime::Role::Owner;
    static constexpr bool IsTrackable = true;
};

template <>
struct Lifetime<Test::Basis::OtnSafe, Test::Ownership::Shared>
{
    static constexpr auto Role        = Test::Lifetime::Role::Owner;
    static constexpr bool IsTrackable = true;
};

template <>
struct Lifetime<Test::Basis::OtnSafe, Test::Ownership::Weak>
{
    static constexpr auto Role        = Test::Lifetime::Role::Observer;
    static constexpr bool IsTrackable = true;
};

template <>
inline constexpr bool
IsExpirable<Test::Basis::OtnSafe, Test::Ownership::Weak> = true;
// ----- Lifetime --------------------------------------------------------------

// ----- Access ----------------------------------------------------------------
template <>
inline constexpr bool
IsDereferenceable<Test::Basis::OtnSafe, Test::Ownership::Weak> = false;

template <>
inline constexpr bool
IsProxyAccessible<Test::Basis::OtnSafe, Test::Ownership::Weak> = true;
// ----- Access ----------------------------------------------------------------

// ----- IsPointerConstructible ------------------------------------------------
template <>
inline constexpr bool
IsPointerConstructible<Test::Basis::OtnSafe, Test::Ownership::Unified> = false;

template <>
inline constexpr bool
IsPointerConstructible<Test::Basis::OtnSafe, Test::Ownership::Unique> = true;

template <>
inline constexpr bool
IsPointerConstructible<Test::Basis::OtnSafe, Test::Ownership::Shared> = true;

template <>
inline constexpr bool
IsPointerConstructible<Test::Basis::OtnSafe, Test::Ownership::Weak> = false;
// ----- IsPointerConstructible ------------------------------------------------

// ----- IsItselfConstructible -------------------------------------------------
template <>
inline constexpr bool
IsItselfConstructible<Test::Basis::OtnSafe, Test::Ownership::Unified> = false;

template <>
inline constexpr bool
IsItselfConstructible<Test::Basis::OtnSafe, Test::Ownership::Unique> = true;

template <>
inline constexpr bool
IsItselfConstructible<Test::Basis::OtnSafe, Test::Ownership::Shared> = true;

template <>
inline constexpr bool
IsItselfConstructible<Test::Basis::OtnSafe, Test::Ownership::Weak> = false;
// ----- IsItselfConstructible -------------------------------------------------

// ----- Copy construction & assignment ----------------------------------------
// ----- OtnSafe <- OtnSafe ----------------------------------------------------
// OtnSafe::UnifiedAny<T> <- OtnSafe::AnySome<Y>
template <class TM,
          class YM>
struct
CanCopy<Test::Basis::OtnSafe, Test::Ownership::Unified, TM,
        Test::Basis::OtnSafe, Test::Ownership::Unified, YM> : ImplicitCopy {};

template <class TM,
          class YM>
struct
CanCopy<Test::Basis::OtnSafe, Test::Ownership::Unified, TM,
        Test::Basis::OtnSafe, Test::Ownership::Unique, YM> : ImplicitCopy {};

template <class TM,
          class YM>
struct
CanCopy<Test::Basis::OtnSafe, Test::Ownership::Unified, TM,
        Test::Basis::OtnSafe, Test::Ownership::Shared, YM> : ImplicitCopy {};

template <class YM>
struct
CanCopy<Test::Basis::OtnSafe, Test::Ownership::Unified, Test::Multiplicity::Optional,
        Test::Basis::OtnSafe, Test::Ownership::Weak, YM> : ImplicitCopy {};

// OtnSafe::SharedAny<T> <- OtnSafe::SharedAny<Y>
template <class TM,
          class YM>
struct
CanCopy<Test::Basis::OtnSafe, Test::Ownership::Shared, TM,
        Test::Basis::OtnSafe, Test::Ownership::Shared, YM> : ImplicitCopy {};

// OtnSafe::WeakAny<T> <- OtnSafe::SomeAny<Y>
template <class TM,
          class YM>
struct
CanCopy<Test::Basis::OtnSafe, Test::Ownership::Weak, TM,
        Test::Basis::OtnSafe, Test::Ownership::Unique, YM> : ImplicitCopy {};

template <class TM,
          class YM>
struct
CanCopy<Test::Basis::OtnSafe, Test::Ownership::Weak, TM,
        Test::Basis::OtnSafe, Test::Ownership::Shared, YM> : ImplicitCopy {};

template <class TM,
          class YM>
struct
CanCopy<Test::Basis::OtnSafe, Test::Ownership::Weak, TM,
        Test::Basis::OtnSafe, Test::Ownership::Weak, YM> : ImplicitCopy {};
// ----- OtnSafe <- OtnSafe ----------------------------------------------------
// ----- Copy construction & assignment ----------------------------------------

// ----- Move construction & assignment ----------------------------------------
// ----- OtnSafe <- OtnSafe ----------------------------------------------------
// OtnSafe::UnifiedAny<T> <- OtnSafe::AnyAny<Y>
template <class TM,
          class YM>
struct
CanMove<Test::Basis::OtnSafe, Test::Ownership::Unified, TM,
        Test::Basis::OtnSafe, Test::Ownership::Unified, YM> : ImplicitMove {};

template <class TM,
          class YM>
struct
CanMove<Test::Basis::OtnSafe, Test::Ownership::Unified, TM,
        Test::Basis::OtnSafe, Test::Ownership::Unique, YM> : ImplicitMove {};

template <class TM,
          class YM>
struct
CanMove<Test::Basis::OtnSafe, Test::Ownership::Unified, TM,
        Test::Basis::OtnSafe, Test::Ownership::Shared, YM> : ImplicitMove {};

template <class YM>
struct
CanMove<Test::Basis::OtnSafe, Test::Ownership::Unified, Test::Multiplicity::Optional,
        Test::Basis::OtnSafe, Test::Ownership::Weak, YM> : ImplicitMove {};

// OtnSafe::UniqueAny<T> <- OtnSafe::UniqueAny<Y>
template <class TM,
          class YM>
struct
CanMove<Test::Basis::OtnSafe, Test::Ownership::Unique, TM,
        Test::Basis::OtnSafe, Test::Ownership::Unique, YM> : ImplicitMove {};

// OtnSafe::SharedAny<T> <- OtnSafe::SomeAny<Y>
template <class TM,
          class YM>
struct
CanMove<Test::Basis::OtnSafe, Test::Ownership::Shared, TM,
        Test::Basis::OtnSafe, Test::Ownership::Unique, YM> : ImplicitMove {};

template <class TM,
          class YM>
struct
CanMove<Test::Basis::OtnSafe, Test::Ownership::Shared, TM,
        Test::Basis::OtnSafe, Test::Ownership::Shared, YM> : ImplicitMove {};

// OtnSafe::WeakAny<T> <- OtnSafe::WeakAny<Y>
template <class TM,
          class YM>
struct
CanMove<Test::Basis::OtnSafe, Test::Ownership::Weak, TM,
        Test::Basis::OtnSafe, Test::Ownership::Weak, YM> : ImplicitMove {};
// ----- OtnSafe <- OtnSafe ----------------------------------------------------
// ----- Move construction & assignment ----------------------------------------

// ----- IsOwnerBased ----------------------------------------------------------
template <class Ownership>
inline constexpr bool
IsOwnerBased<Test::Basis::OtnSafe, Ownership> = true;
// ----- IsOwnerBased ----------------------------------------------------------

// ----- HasFeature ------------------------------------------------------------
template <class Ownership, class Multiplicity>
inline constexpr bool
HasFeature<Test::Basis::OtnSafe, Ownership, Multiplicity,
           Test::Feature::IsRange> = true;

template <class Ownership, class Multiplicity>
inline constexpr bool
HasFeature<Test::Basis::OtnSafe, Ownership, Multiplicity,
           Test::Feature::ContractViolation::Abort> = true;
// ----- HasFeature ------------------------------------------------------------

} // namespace Rule

} // namespace Internal

} // namespace Bind

} // namespace Test
