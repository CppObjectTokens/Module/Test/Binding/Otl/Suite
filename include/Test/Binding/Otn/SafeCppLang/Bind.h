/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <Test/Token/Bind.h>

#include <Test/Binding/Otn/Safe/Basis.h>
#include <Test/Binding/Otn/CppLang/Basis.h>

namespace Test
{

namespace Bind
{

namespace Internal
{

namespace Rule
{

// ----- Copy construction & assignment ----------------------------------------
// ----- OtnSafe <- CppLang ----------------------------------------------------
// OtnSafe::UniqueAny<T> <- CppLang::UnifiedUnknown<Y>
template <class TM>
struct
CanCopy<Test::Basis::OtnSafe, Test::Ownership::Unique, TM,
        Test::Basis::CppLang, Test::Ownership::Unified, Test::Multiplicity::Unknown> : ExplicitCopy {};

// OtnSafe::SharedAny<T> <- CppLang::UnifiedUnknown<Y>
template <class TM>
struct
CanCopy<Test::Basis::OtnSafe, Test::Ownership::Shared, TM,
        Test::Basis::CppLang, Test::Ownership::Unified, Test::Multiplicity::Unknown> : ExplicitCopy {};
// ----- OtnSafe <- CppLang ----------------------------------------------------

// ----- CppLang <- OtnSafe ----------------------------------------------------
// CppLang::UnifiedUnknown<T> <- OtnSafe::SomeAny<Y>
template <class YM>
struct
CanCopy<Test::Basis::CppLang, Test::Ownership::Unified, Test::Multiplicity::Unknown,
        Test::Basis::OtnSafe, Test::Ownership::Unified, YM> : ExplicitCopy {};

template <class YM>
struct
CanCopy<Test::Basis::CppLang, Test::Ownership::Unified, Test::Multiplicity::Unknown,
        Test::Basis::OtnSafe, Test::Ownership::Unique, YM> : ExplicitCopy {};

template <class YM>
struct
CanCopy<Test::Basis::CppLang, Test::Ownership::Unified, Test::Multiplicity::Unknown,
        Test::Basis::OtnSafe, Test::Ownership::Shared, YM> : ExplicitCopy {};

template <class YM>
struct
CanCopy<Test::Basis::CppLang, Test::Ownership::Unified, Test::Multiplicity::Unknown,
        Test::Basis::OtnSafe, Test::Ownership::Weak, YM> : ExplicitCopy {};

// CppLang::UnifiedOptional<T> <- OtnSafe::SomeAny<Y>
template <class YM>
struct
CanCopy<Test::Basis::CppLang, Test::Ownership::Unified, Test::Multiplicity::Optional,
        Test::Basis::OtnSafe, Test::Ownership::Unified, YM> : ImplicitCopy {};

template <class YM>
struct
CanCopy<Test::Basis::CppLang, Test::Ownership::Unified, Test::Multiplicity::Optional,
        Test::Basis::OtnSafe, Test::Ownership::Unique, YM> : ImplicitCopy {};

template <class YM>
struct
CanCopy<Test::Basis::CppLang, Test::Ownership::Unified, Test::Multiplicity::Optional,
        Test::Basis::OtnSafe, Test::Ownership::Shared, YM> : ImplicitCopy {};

template <class YM>
struct
CanCopy<Test::Basis::CppLang, Test::Ownership::Unified, Test::Multiplicity::Optional,
        Test::Basis::OtnSafe, Test::Ownership::Weak, YM> : ImplicitCopy {};
// ----- CppLang <- OtnSafe ----------------------------------------------------
// ----- Copy construction & assignment ----------------------------------------

// ----- Move construction & assignment ----------------------------------------
// ----- OtnSafe <- CppLang ----------------------------------------------------
// OtnSafe::UniqueAny<T> <- CppLang::UnifiedUnknown<Y>
template <class TM>
struct
CanMove<Test::Basis::OtnSafe, Test::Ownership::Unique, TM,
        Test::Basis::CppLang, Test::Ownership::Unified, Test::Multiplicity::Unknown> : ExplicitMoveUnspecified {};

// OtnSafe::SharedAny<T> <- CppLang::UnifiedUnknown<Y>
template <class TM>
struct
CanMove<Test::Basis::OtnSafe, Test::Ownership::Shared, TM,
        Test::Basis::CppLang, Test::Ownership::Unified, Test::Multiplicity::Unknown> : ExplicitMoveUnspecified {};
// ----- OtnSafe <- CppLang ----------------------------------------------------

// ----- CppLang <- OtnSafe ----------------------------------------------------
// CppLang::UnifiedUnknown<Y> <- OtnSafe::WeakAny<T>
template <class YM>
struct
CanMove<Test::Basis::CppLang, Test::Ownership::Unified, Test::Multiplicity::Unknown,
        Test::Basis::OtnSafe, Test::Ownership::Weak, YM> : ExplicitMove {};

// CppLang::UnifiedOptional<Y> <- OtnSafe::WeakAny<T>
template <class YM>
struct
CanMove<Test::Basis::CppLang, Test::Ownership::Unified, Test::Multiplicity::Optional,
        Test::Basis::OtnSafe, Test::Ownership::Weak, YM> : ImplicitMove {};
// ----- CppLang <- OtnSafe ----------------------------------------------------
// ----- Move construction & assignment ----------------------------------------

} // namespace Rule

} // namespace Internal

} // namespace Bind

} // namespace Test
