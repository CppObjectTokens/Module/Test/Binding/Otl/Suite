/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <Test/Token/Bind.h>

#include <Test/Binding/Otn/Safe/Basis.h>
#include <Test/Binding/Otn/Raw/Basis.h>

namespace Test
{

namespace Bind
{

namespace Internal
{

namespace Rule
{

// ----- Copy construction & assignment ----------------------------------------
// ----- OtnRaw <- OtnSafe -----------------------------------------------------
// OtnRaw::UnifiedAny<Y> <- OtnSafe::AnyAny<T>
template <class TM,
          class YO, class YM>
struct
CanCopy<Test::Basis::OtnRaw, Test::Ownership::Unified, TM,
        Test::Basis::OtnSafe, YO, YM> : ImplicitCopy {};

// OtnRaw::WeakAny<Y> <- OtnSafe::SomeAny<T>
template <class TM,
          class YM>
struct
CanCopy<Test::Basis::OtnRaw, Test::Ownership::Weak, TM,
        Test::Basis::OtnSafe, Test::Ownership::Unique, YM> : ImplicitCopy {};

template <class TM,
          class YM>
struct
CanCopy<Test::Basis::OtnRaw, Test::Ownership::Weak, TM,
        Test::Basis::OtnSafe, Test::Ownership::Shared, YM> : ImplicitCopy {};

template <class TM,
          class YM>
struct
CanCopy<Test::Basis::OtnRaw, Test::Ownership::Weak, TM,
        Test::Basis::OtnSafe, Test::Ownership::Weak, YM> : ImplicitCopy {};
// ----- OtnRaw <- OtnSafe -----------------------------------------------------
// ----- Copy construction & assignment ----------------------------------------

// ----- Move construction & assignment ----------------------------------------
// ----- OtnSafe <- OtnRaw -----------------------------------------------------
// OtnSafe::UnifiedAny<T> <- OtnRaw::UniqueSome<Y>
template <class TM>
struct
CanMove<Test::Basis::OtnSafe, Test::Ownership::Unified, TM,
        Test::Basis::OtnRaw, Test::Ownership::Unique, Test::Multiplicity::Optional> : ImplicitMove
{
    static constexpr bool IsRelocateObject = true;
};

template <class TM>
struct
CanMove<Test::Basis::OtnSafe, Test::Ownership::Unified, TM,
        Test::Basis::OtnRaw, Test::Ownership::Unique, Test::Multiplicity::Single> : ImplicitMoveRelocateObject {};

// OtnSafe::UniqueAny<T> <- OtnRaw::UniqueSome<Y>
template <class TM>
struct
CanMove<Test::Basis::OtnSafe, Test::Ownership::Unique, TM,
        Test::Basis::OtnRaw, Test::Ownership::Unique, Test::Multiplicity::Optional> : ImplicitMove
{
    static constexpr bool IsRelocateObject = true;
};

template <class TM>
struct
CanMove<Test::Basis::OtnSafe, Test::Ownership::Unique, TM,
        Test::Basis::OtnRaw, Test::Ownership::Unique, Test::Multiplicity::Single> : ImplicitMoveRelocateObject {};

// OtnSafe::SharedAny<T> <- OtnRaw::UniqueSome<Y>
template <class TM>
struct
CanMove<Test::Basis::OtnSafe, Test::Ownership::Shared, TM,
        Test::Basis::OtnRaw, Test::Ownership::Unique, Test::Multiplicity::Optional> : ImplicitMove
{
    static constexpr bool IsRelocateObject = true;
};

template <class TM>
struct
CanMove<Test::Basis::OtnSafe, Test::Ownership::Shared, TM,
        Test::Basis::OtnRaw, Test::Ownership::Unique, Test::Multiplicity::Single> : ImplicitMoveRelocateObject {};
// ----- OtnSafe <- OtnRaw -----------------------------------------------------

// ----- OtnRaw <- OtnSafe -----------------------------------------------------
// OtnRaw::UnifiedAny<Y> <- OtnSafe::WeakAny<T>
template <class TM,
          class YM>
struct
CanMove<Test::Basis::OtnRaw, Test::Ownership::Unified, TM,
        Test::Basis::OtnSafe, Test::Ownership::Weak, YM> : ImplicitMove {};

// OtnRaw::WeakAny<Y> <- OtnSafe::WeakAny<T>
template <class TM,
          class YM>
struct
CanMove<Test::Basis::OtnRaw, Test::Ownership::Weak, TM,
        Test::Basis::OtnSafe, Test::Ownership::Weak, YM> : ImplicitMove {};
// ----- OtnRaw <- OtnSafe -----------------------------------------------------
// ----- Move construction & assignment ----------------------------------------

} // namespace Rule

} // namespace Internal

} // namespace Bind

} // namespace Test
