/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <Test/Token/Bind.h>

#include <Test/Binding/Otn/Safe/Basis.h>
#include <Test/Binding/StdSmart/Origin/Basis.h>

namespace Test
{

namespace Bind
{

namespace Internal
{

namespace Rule
{

// ----- Copy construction & assignment ----------------------------------------
// ----- OtnSafe <- StdSmart ---------------------------------------------------
// OtnSafe::UnifiedSome<T> <- StdSmart::SomeAny<Y>
template <class TM,
          class YO, class YM>
struct
CanCopy<Test::Basis::OtnSafe, Test::Ownership::Unified, TM,
        Test::Basis::StdSmart, YO, YM>
    : CanCopy<Test::Basis::OtnSafe, Test::Ownership::Shared, TM,
              Test::Basis::StdSmart, YO, YM> {};

// OtnSafe::SharedSome<T> <- StdSmart::SomeAny<Y>
template <class TM,
          class YM>
struct
CanCopy<Test::Basis::OtnSafe, Test::Ownership::Shared, TM,
        Test::Basis::StdSmart, Test::Ownership::Shared, YM> : ImplicitCopy {};

template <class YM>
struct
CanCopy<Test::Basis::OtnSafe, Test::Ownership::Shared, Test::Multiplicity::Optional,
        Test::Basis::StdSmart, Test::Ownership::Weak, YM> : ImplicitCopy {};

// OtnSafe::WeakAny<T> <- StdSmart::SomeAny<Y>
template <class TM,
          class YM>
struct
CanCopy<Test::Basis::OtnSafe, Test::Ownership::Weak, TM,
        Test::Basis::StdSmart, Test::Ownership::Shared, YM> : ImplicitCopy {};

template <class TM,
          class YM>
struct
CanCopy<Test::Basis::OtnSafe, Test::Ownership::Weak, TM,
        Test::Basis::StdSmart, Test::Ownership::Weak, YM> : ImplicitCopy {};
// ----- OtnSafe <- StdSmart ---------------------------------------------------

// ----- StdSmart <- OtnSafe ---------------------------------------------------
// StdSmart::SharedAny<Y> <- OtnSafe::SharedAny<T>
template <class TM,
          class YM>
struct
CanCopy<Test::Basis::StdSmart, Test::Ownership::Shared, TM,
        Test::Basis::OtnSafe, Test::Ownership::Shared, YM> : ImplicitCopy {};

// StdSmart::WeakAny<Y> <- OtnSafe::SharedAny<T>
template <class TM,
          class YM>
struct
CanCopy<Test::Basis::StdSmart, Test::Ownership::Weak, TM,
        Test::Basis::OtnSafe, Test::Ownership::Shared, YM> : ImplicitCopy {};
// ----- StdSmart <- OtnSafe ---------------------------------------------------
// ----- Copy construction & assignment ----------------------------------------

// ----- Move construction & assignment ----------------------------------------
// ----- OtnSafe <- StdSmart ---------------------------------------------------
// OtnSafe::UnifiedAny<T> <- StdSmart::SharedAny<Y>
template <class TM,
          class YO, class YM>
struct
CanMove<Test::Basis::OtnSafe, Test::Ownership::Unified, TM,
        Test::Basis::StdSmart, YO, YM>
    : CanMove<Test::Basis::OtnSafe, Test::Ownership::Shared, TM,
              Test::Basis::StdSmart, YO, YM> {};

// OtnSafe::UniqueAny<T> <- StdSmart::UniqueAny<Y>
template <class TM,
          class YM>
struct
CanMove<Test::Basis::OtnSafe, Test::Ownership::Unique, TM,
        Test::Basis::StdSmart, Test::Ownership::Unique, YM> : ImplicitMove {};

// OtnSafe::SharedSome<T> <- StdSmart::SomeSome<Y>
template <class TM,
          class YM>
struct
CanMove<Test::Basis::OtnSafe, Test::Ownership::Shared, TM,
        Test::Basis::StdSmart, Test::Ownership::Unique, YM> : ImplicitMove {};

template <class TM,
          class YM>
struct
CanMove<Test::Basis::OtnSafe, Test::Ownership::Shared, TM,
        Test::Basis::StdSmart, Test::Ownership::Shared, YM> : ImplicitMove {};

template <class YM>
struct
CanMove<Test::Basis::OtnSafe, Test::Ownership::Shared, Test::Multiplicity::Optional,
        Test::Basis::StdSmart, Test::Ownership::Weak, YM> : ImplicitMove {};

// OtnSafe::WeakAny<T> <- StdSmart::WeakAny<Y>
template <class TM,
          class YM>
struct
CanMove<Test::Basis::OtnSafe, Test::Ownership::Weak, TM,
        Test::Basis::StdSmart, Test::Ownership::Weak, YM> : ImplicitMove {};
// ----- OtnSafe <- StdSmart ---------------------------------------------------

// ----- StdSmart <- OtnSafe ---------------------------------------------------
// StdSmart::SharedAny<Y> <- OtnSafe::SharedAny<T>
template <class TM,
          class YM>
struct
CanMove<Test::Basis::StdSmart, Test::Ownership::Shared, TM,
        Test::Basis::OtnSafe, Test::Ownership::Shared, YM> : ImplicitMove {};
// ----- StdSmart <- OtnSafe ---------------------------------------------------
// ----- Move construction & assignment ----------------------------------------

} // namespace Rule

} // namespace Internal

} // namespace Bind

} // namespace Test
