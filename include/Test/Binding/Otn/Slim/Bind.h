/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <Test/Token/Lifetime.h>
#include <Test/Token/Feature.h>

#include <Test/Binding/Otn/Gateway.h>
#include <Test/Binding/Otn/Slim/Basis.h>

#include <otn/slim.hpp>

namespace Test
{

namespace Bind
{

namespace Internal
{

// ----- Itself ----------------------------------------------------------------
template <>
struct Itself<Test::Basis::OtnSlim>
{
    using Type = otn::itself_t;
    static constexpr Type Value = otn::itself;
};

template <class Element>
struct ItselfType<Element, Test::Basis::OtnSlim>
{
    using Type = otn::itself_type_t<Element>;
    static constexpr Type Value = otn::itself_type<Element>;
};
// ----- Itself ----------------------------------------------------------------

// ----- Instance --------------------------------------------------------------
template <class Element, class Ownership, class Multiplicity>
struct Instance<Element, Test::Basis::OtnSlim, Ownership, Multiplicity>
{
    using Type = otn::generic::token<Element,
                                     otn::basis::slim,
                                     Bind::Gateway::Outer<Ownership>,
                                     Bind::Gateway::Outer<Multiplicity>>;
};
// ----- Instance --------------------------------------------------------------

namespace Rule
{

// ----- Spec ------------------------------------------------------------------
template <class Token>
struct Spec<Token, IsInstanceOf<Token,
                                Test::Basis::OtnSlim,
                                Test::Ownership::Unique,
                                Test::Multiplicity::Optional>>
    : TokenSpec<Token,
                Test::Basis::OtnSlim,
                Test::Ownership::Unique,
                Test::Multiplicity::Optional> {};

template <class Token>
struct Spec<Token, IsInstanceOf<Token,
                                Test::Basis::OtnSlim,
                                Test::Ownership::Unique,
                                Test::Multiplicity::Single>>
    : TokenSpec<Token,
                Test::Basis::OtnSlim,
                Test::Ownership::Unique,
                Test::Multiplicity::Single> {};
// ----- Spec ------------------------------------------------------------------

// ----- Lifetime --------------------------------------------------------------
template <>
struct Lifetime<Test::Basis::OtnSlim, Test::Ownership::Unique>
{
    static constexpr auto Role        = Test::Lifetime::Role::Owner;
    static constexpr bool IsTrackable = false;
};
// ----- Lifetime --------------------------------------------------------------

// ----- IsPointerConstructible ------------------------------------------------
template <class Ownership>
inline constexpr bool
IsPointerConstructible<Test::Basis::OtnSlim, Ownership> = true;
// ----- IsPointerConstructible ------------------------------------------------

// ----- IsItselfConstructible -------------------------------------------------
template <>
inline constexpr bool
IsItselfConstructible<Test::Basis::OtnSlim, Test::Ownership::Unique> = true;
// ----- IsItselfConstructible -------------------------------------------------

// ----- Copy construction & assignment ----------------------------------------
// ----- Copy construction & assignment ----------------------------------------

// ----- Move construction & assignment ----------------------------------------
// ----- OtnSlim <- OtnSlim ----------------------------------------------------
// OtnSlim::UniqueAny<T> <- OtnSlim::UniqueAny<Y>
template <class TM,
          class YM>
struct
CanMove<Test::Basis::OtnSlim, Test::Ownership::Unique, TM,
        Test::Basis::OtnSlim, Test::Ownership::Unique, YM> : ImplicitMove {};
// ----- OtnSlim <- OtnSlim ----------------------------------------------------
// ----- Move construction & assignment ----------------------------------------

// ----- HasFeature ------------------------------------------------------------
template <class Ownership, class Multiplicity>
inline constexpr bool
HasFeature<Test::Basis::OtnSlim, Ownership, Multiplicity,
           Test::Feature::IsRange> = true;

template <class Ownership, class Multiplicity>
inline constexpr bool
HasFeature<Test::Basis::OtnSlim, Ownership, Multiplicity,
           Test::Feature::ContractViolation::Abort> = true;
// ----- HasFeature ------------------------------------------------------------

} // namespace Rule

} // namespace Internal

} // namespace Bind

} // namespace Test
