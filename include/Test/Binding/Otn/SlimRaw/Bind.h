/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <Test/Token/Bind.h>

#include <Test/Binding/Otn/Slim/Basis.h>
#include <Test/Binding/Otn/Raw/Basis.h>

namespace Test
{

namespace Bind
{

namespace Internal
{

namespace Rule
{

// ----- Copy construction & assignment ----------------------------------------
// ----- OtnRaw <- OtnSlim -----------------------------------------------------
// OtnRaw::UnifiedAny<T> <- OtnSlim::SomeAny<Y>
template <class TM,
          class YM>
struct
CanCopy<Test::Basis::OtnRaw, Test::Ownership::Unified, TM,
        Test::Basis::OtnSlim, Test::Ownership::Unique, YM> : ImplicitCopy {};

// OtnRaw::WeakAny<T> <- OtnSlim::UniqueAny<Y>
template <class TM,
          class YM>
struct
CanCopy<Test::Basis::OtnRaw, Test::Ownership::Weak, TM,
        Test::Basis::OtnSlim, Test::Ownership::Unique, YM> : ImplicitCopy {};
// ----- OtnRaw <- OtnSlim -----------------------------------------------------
// ----- Copy construction & assignment ----------------------------------------

// ----- Move construction & assignment ----------------------------------------
// ----- OtnSlim <- OtnRaw -----------------------------------------------------
// OtnSlim::UniqueAny<T> <- OtnRaw::UniqueSome<Y>
template <class TM>
struct
CanMove<Test::Basis::OtnSlim, Test::Ownership::Unique, TM,
        Test::Basis::OtnRaw, Test::Ownership::Unique, Test::Multiplicity::Optional> : ImplicitMove
{
    static constexpr bool IsRelocateObject = true;
};

template <class TM>
struct
CanMove<Test::Basis::OtnSlim, Test::Ownership::Unique, TM,
        Test::Basis::OtnRaw, Test::Ownership::Unique, Test::Multiplicity::Single> : ImplicitMoveRelocateObject {};
// ----- OtnSlim <- OtnRaw -----------------------------------------------------

// ----- OtnRaw <- OtnSlim ----------------------------------------------------
// OtnRaw::UniqueAny<T> <- OtnSlim::UniqueAny<Y>
template <class TM,
          class YM>
struct
CanMove<Test::Basis::OtnRaw, Test::Ownership::Unique, TM,
        Test::Basis::OtnSlim, Test::Ownership::Unique, YM> : ImplicitMove
{
    static constexpr bool IsRelocateObject = true;
};
// ----- OtnRaw <- OtnSlim ----------------------------------------------------
// ----- Move construction & assignment ----------------------------------------

} // namespace Rule

} // namespace Internal

} // namespace Bind

} // namespace Test
