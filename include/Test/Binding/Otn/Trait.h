/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <Test/Token/Bind.h>

#include <otn/v1/element/traits.hpp>
#include <otn/v1/basis/traits.hpp>
#include <otn/v1/ownership/traits.hpp>
#include <otn/v1/multiplicity/traits.hpp>
#include <otn/v1/deleter/traits.hpp>
#include <otn/v1/lifetime/traits.hpp>
#include <otn/v1/referrer/traits.hpp>
#include <otn/v1/base/traits.hpp>

namespace Test
{

namespace Bind
{

namespace Internal
{

namespace Trait
{

template <class Token>
struct Element<Token>
{ using Type = otn::traits::element_t<Token>; };

template <class Token>
struct Basis<Token>
{ using Type = otn::traits::basis_t<Token>; };

template <class Token>
struct Ownership<Token>
{ using Type = otn::traits::ownership_t<Token>; };

template <class Token>
struct Multiplicity<Token>
{ using Type = otn::traits::multiplicity_t<Token>; };

template <class Token>
struct Deleter<Token>
{ using Type = otn::traits::deleter_t<Token>; };

namespace Lifetime
{

template <class Token>
struct Role<Token>
{ static constexpr auto Value = otn::traits::lifetime::role_v<Token>; };

} // namespace Lifetime

template <class Token>
struct IsToken<Token>
{ static constexpr bool Value = otn::is_token_v<Token>; };

template <class Token>
struct IsOwner<Token>
{ static constexpr bool Value = otn::is_owner_v<Token>; };

template <class Token>
struct IsObserver<Token>
{ static constexpr bool Value = otn::is_observer_v<Token>; };

template <class Token>
struct IsOptional<Token>
{ static constexpr bool Value = otn::is_optional_v<Token>; };

template <class Token>
struct IsSingle<Token>
{ static constexpr bool Value = otn::is_single_v<Token>; };

template <class Token>
struct IsTrackable<Token>
{ static constexpr bool Value = otn::is_trackable_v<Token>; };

template <class Token>
struct IsDereferenceable<Token>
{ static constexpr bool Value = otn::is_dereferenceable_v<Token>; };

template <class Token>
struct IsDirectAccessible<Token>
{ static constexpr bool Value = otn::is_direct_accessible_v<Token>; };

template <class Token>
struct IsProxyAccessible<Token>
{ static constexpr bool Value = otn::is_proxy_accessible_v<Token>; };

} // namespace Trait

} // namespace Internal

} // namespace Bind

} // namespace Test
